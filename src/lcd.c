
#include <xc.h>
#include <math.h>
#include "lcd.h"
#include "global.h"

void i2c_start()
{
    SSP1IF = 0;
    SEN = 1;
    while(!SSP1IF) NOP();
    SSP1IF = 0;
}

void i2c_send_stop()
{
    SSP1IF = 0;
    PEN = 1;
    while(!SSP1IF) NOP();
    SSP1IF = 0;
}

int i2c_send_byte(uint8_t adress, uint8_t data)
{
    i2c_start();
    SSP1BUF = (uint8_t) (adress << 1); // last bis is WnR
    while(!SSP1IF) NOP();
    SSP1IF = 0;
    if(ACKSTAT)
    {
        i2c_send_stop();
        return -2;
    }
    SSP1BUF = data;
    while(!SSP1IF) NOP();
    SSP1IF = 0;
    if(ACKSTAT)
    {
        i2c_send_stop();
        return -1;
    }
    i2c_send_stop();
    return 0;
}

int i2c_send_bytes(uint8_t adress, uint8_t length, uint8_t* data)
{
    i2c_start();
    SSP1BUF = (uint8_t) (adress << 1); // last bis is WnR
    while(!SSP1IF) NOP();
    SSP1IF = 0;
    if(ACKSTAT)
    {
        i2c_send_stop();
        return -2;
    }
    for(int i=0; i<length; i++)
    {
        SSP1BUF = data[i];
        while(!SSP1IF) NOP();
        SSP1IF = 0;
        if(ACKSTAT)
        {
            i2c_send_stop();
            return -1;
        }
    }
    i2c_send_stop();
    return 0;
}

void lcd_send_enable(uint8_t half_byte)
{
    uint8_t data[3];
    data[0] = (uint8_t) (half_byte<<4 | LCD_ENABLE_BACKLIGHT<<3);
    data[1] = (uint8_t) (data[0] | 1<<2);
    data[2] = data[0];
    
    i2c_send_bytes(LCD_ADRESS, 3, data);
}
    
void lcd_send_byte(uint8_t byte, uint8_t RS)
{
    uint8_t data[6];
    data[0] = (uint8_t) ( (byte&0xF0) | LCD_ENABLE_BACKLIGHT<<3 | RS);
    data[1] = (uint8_t) (data[0] | 1<<2);
    data[2] = data[0];
    data[3] = (uint8_t) (byte<<4 | LCD_ENABLE_BACKLIGHT<<3 | RS);
    data[4] = (uint8_t) (data[3] | 1<<2);
    data[5] = data[3];
    
    i2c_send_bytes(LCD_ADRESS, 6, data);
}

void lcd_command(uint8_t byte)
{
    lcd_send_byte(byte, 0);
}

void lcd_data(uint8_t byte)
{
    lcd_send_byte(byte, 1);
}

void lcd_init()
{
    __delay_ms(LCD_BOOTUP_MS);
    lcd_send_enable(LCD_SOFT_RESET);
    __delay_ms(LCD_SOFT_RESET_MS1);
    lcd_send_enable(LCD_SOFT_RESET);
    __delay_us(LCD_SOFT_RESET_US2);
    lcd_send_enable(LCD_SOFT_RESET);
    lcd_send_enable(LCD_SETUP_4_BIT);
    
    lcd_command( LCD_SET_FUNCTION |
                 LCD_FUNCTION_4BIT |
                 LCD_FUNCTION_2LINE |
                 LCD_FUNCTION_5X7 );
 
    lcd_command( LCD_SET_DISPLAY |
                 LCD_DISPLAY_ON |
                 LCD_CURSOR_OFF |
                 LCD_BLINKING_OFF); 
 
    lcd_command( LCD_SET_ENTRY |
                 LCD_ENTRY_INCREASE |
                 LCD_ENTRY_NOSHIFT );
 
    lcd_clear();
}

void lcd_home()
{
    lcd_command( LCD_CURSOR_HOME );
    __delay_ms( LCD_CURSOR_HOME_MS );
}

void lcd_clear()
{
    lcd_command( LCD_CLEAR_DISPLAY );
    __delay_ms( LCD_CLEAR_DISPLAY_MS );
}

void lcd_string( const char *data )
{
    while( *data != '\0' )
        lcd_data( *data++ );
}

void lcd_setcursor( uint8_t x, uint8_t y )
{
    uint8_t data;
 
    switch (y)
    {
        case 1: 
            data = LCD_SET_DDADR + LCD_DDADR_LINE1 + x;
            break;
        case 2:
            data = LCD_SET_DDADR + LCD_DDADR_LINE2 + x;
            break;
        default:
            return; 
    }
 
    lcd_command( data );
}

void lcd_number(uint8_t number)
{
    while (number > 99)
    {
        number = number-100;
    }
    char str[3] = "  ";
    str[1] = number - (number/10)*10;
    if (number > 9)
    {
        str[0] = (number-str[1])/10 + '0';
    }
    str[1] += '0';
    lcd_string(str);
}



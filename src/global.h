/* 
 * File:   global.h
 * Author: Michael
 *
 * Created on February 23, 2021, 10:27 PM
 */

#ifndef GLOBAL_H
#define	GLOBAL_H

#ifdef	__cplusplus
extern "C" {
#endif

#define _XTAL_FREQ 500000



#ifdef	__cplusplus
}
#endif

#endif	/* GLOBAL_H */


/*
 * File:   main.c
 * Author: Michael
 *
 * Created on February 21, 2021, 10:30 AM
 */

// 'C' source line config statements

// CONFIG1
#pragma config FOSC = INTOSC    // Oscillator Selection (INTOSC oscillator: I/O function on CLKIN pin)
#pragma config WDTE = OFF       // Watchdog Timer Enable (WDT disabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable (PWRT disabled)
#pragma config MCLRE = ON       // MCLR Pin Function Select (MCLR/VPP pin function is MCLR)
#pragma config CP = OFF         // Flash Program Memory Code Protection (Program memory code protection is disabled)
#pragma config CPD = OFF        // Data Memory Code Protection (Data memory code protection is disabled)
#pragma config BOREN = ON       // Brown-out Reset Enable (Brown-out Reset enabled)
#pragma config CLKOUTEN = OFF   // Clock Out Enable (CLKOUT function is disabled. I/O or oscillator function on the CLKOUT pin)
#pragma config IESO = OFF       // Internal/External Switchover (Internal/External Switchover mode is disabled)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable (Fail-Safe Clock Monitor is disabled)

// CONFIG2
#pragma config WRT = OFF        // Flash Memory Self-Write Protection (Write protection off)
#pragma config PLLEN = OFF      // PLL Enable (4x PLL disabled)
#pragma config STVREN = ON      // Stack Overflow/Underflow Reset Enable (Stack Overflow or Underflow will cause a Reset)
#pragma config BORV = LO        // Brown-out Reset Voltage Selection (Brown-out Reset Voltage (Vbor), low trip point selected.)
#pragma config LVP = OFF        // Low-Voltage Programming Enable (High-voltage on MCLR/VPP must be used for programming)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>
#include <stdint.h>
#include "global.h"
#include "lcd.h"

#define PWM_MAX 20
#define PWM_MIN 4

void device_init();
void setPWM_cycle(unsigned short value);

uint8_t pwm_value = 10;

void __interrupt() isr()
{
    
    // potentially add debouncing
    if(IOCAF4 && IOCAP4)
    {
        if (pwm_value == PWM_MIN) pwm_value = 0;
        else if (pwm_value > PWM_MIN) pwm_value -= 1;
    }
    
    if(IOCAF5 && IOCAP5)
    {
        if (pwm_value == 0) pwm_value = PWM_MIN ;
        else if (pwm_value < PWM_MAX) pwm_value += 1;
    }
    
    lcd_home();
    lcd_clear();
    lcd_number(pwm_value);
    setPWM_cycle(pwm_value);
    
    __delay_ms(150);
    IOCAF4 = 0;
    IOCAF5 = 0;
}

void main() {
    device_init();
    lcd_init();
    lcd_number(pwm_value);
    
    while(1)
    {
        NOP();
    }
}

void device_init()
{
    OSCCON = 0b00111010; // internal clock; 500khz
    
    //todo apfcon
    
    TRISA = 0xFF;
    TRISB = 0xFF;
    TRISC = 0xFF;
    
    ANSELA = 0x00;
    ANSELB = 0x00;
    ANSELC = 0x00;
    
    //PWM on C5/CCP1 with tmr2
    PR2 = 4;
    CCP1CON = 0b00111100;
    setPWM_cycle(pwm_value);
    CCPTMRS0 = (CCPTMRS0 & 0xFB) |  0b00; // select tmr 2 for ccp1
    TMR2IF = 0;
    T2CON = 0;
    TMR2ON = 1;    
    while(TMR2IF == 0) NOP(); 
    TRISC5 = 0;
    
    // IOC on RA4 and RA5
    GIE = 1;
    PEIE = 1;
    IOCIE = 1;
    IOCAF4 = 0;
    IOCAP4 = 1;
    IOCAF5 = 0;
    IOCAP5 = 1;
    
    // I2C on RB4 (SDA) and RB6(SCL)
    SSP1ADD = 3; // ckock is fosc/(3*4)
    SSP1CON2 = 0;
    SSP1CON3 = 0x08;
    SSP1CON1 = 0x38; // i2c master mode, release clock, enable
}

void setPWM_cycle(unsigned short value)
{
    DC1B0 = (__bit)value;
    DC1B1 = (__bit)(value >> 1);
    CCPR1L = (unsigned char)(value >> 2);
}